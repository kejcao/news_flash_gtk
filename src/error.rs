use news_flash::error::NewsFlashError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum NewsFlashGtkError {
    #[error("Error during {:?}: {}", context, source)]
    NewsFlash { source: NewsFlashError, context: String },

    #[error(transparent)]
    Other(#[from] eyre::Report),
}
